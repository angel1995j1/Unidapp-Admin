import { createContext } from 'react';

export const SidebarContext = createContext<any>('');

const SidebarContextWrapper = ({children}:any) => {
    const initialState = { menuItem: '' }

    
    return(
        <SidebarContext.Provider value={{ menuItem: initialState.menuItem }}>
            {children}
        </SidebarContext.Provider>
    )
}


export default SidebarContextWrapper;