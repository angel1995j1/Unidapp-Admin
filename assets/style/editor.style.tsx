import styled from "styled-components";

const QuillEditor = styled.div`
  .ql-editor {
    min-height: 215px;
    text-align: 'left';
  }

  .ql-toolbar.ql-snow,
  .ql-container.ql-snow {
    border: 1px solid;

    .ql-picker-label {
      padding: '0 2px 0 8px';

      &:not(.ql-color-picker):not(.ql-icon-picker) {
        svg {
          right: '0';
          left: '0';
        }
      }
    }
  }

  .ql-container {
    min-height: 220px;
  }
`;

export default QuillEditor;
