import React from "react";
// components

import Navbar from "@components/AuthNavbar";

export default function Auth({ children }:any) {
  return (
    <>
      <Navbar transparent />
      <main>
        <section className="relative w-full h-full py-40 min-h-screen">
          <div
            className="absolute top-0 w-full h-full bg-blueGray-800 bg-no-repeat bg-full"
            style={{
              backgroundImage: "url('/img/background.png')",
            }}
          ></div>
          {children}
        </section>
      </main>
    </>
  );
}
