import React from 'react';

import AdminNavbar from '@components/AdminNavbar';
import Sidebar from '@components/Sidebar';


export default function Admin({children}:any){
    return(
        <>
            <Sidebar />
            <div className="relative md:ml-64 bg-blueGray-100">
                <AdminNavbar />
                <div className="px-4 md:px-10 mx-auto w-full -m-24">
                    {children}
                </div>
            </div>
        </>
    );
}