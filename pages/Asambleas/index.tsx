import React from "react";
import withAuth from "@helpers/private_route";
import Asamblea from '@components/Asambleas';
import Sidebar from "@components/Sidebar";
import Head from "@components/Head";
import { Layout } from "antd";


const Asambleas = ( ) => {
    const { Content } = Layout;

    return(
        <>
        <Layout>
          <Head />
          <Layout>
            <Sidebar>
              <Content>
                <Asamblea />
              </Content>
            </Sidebar>
          </Layout>
        </Layout>
      </>
    )
}


export default withAuth(Asambleas);