import React from "react";
import withAuth from "@helpers/private_route";
import Sidebar from "@components/Sidebar";
import Head from "@components/Head";
import { Layout } from "antd";

import AfiliacionesList from "@components/Afiliaciones";

const Afiliaciones = () => {
  const { Content } = Layout;

  return (
    <>
      <Layout>
        <Head />
        <Layout>
          <Sidebar>
            <Content>
              <AfiliacionesList />
            </Content>
          </Sidebar>
        </Layout>
      </Layout>
    </>
  );
}

export default withAuth(Afiliaciones)