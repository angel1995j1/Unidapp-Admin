import React, { useState } from "react";
import { useRouter } from "next/router";
import axios from "axios";
import { useForm } from "react-hook-form";
import { BoxLoading } from "react-loadingg";
import Auth from "@layouts/Auth";
import Head from "@components/Head";
import logo from "@assets/img/logo.png";
import { message } from 'antd';

import ILogin from "@interfaces/auth/login_interface";

export default function Login() {
  const router = useRouter();
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm();
  const [loading, setLoading] = useState(false);

  const onSubmit = async (data: ILogin) => {
    setLoading(true);
    try {
      const response = await axios.post("https://api-smc43.ondigitalocean.app/api/login", {
        email: data.email,
        password: data.password,
      });

      if (response.status === 200 && response.data.usuario.rol === "Administrador") {
        setLoading(false);
        const token = response.data.token;
        localStorage.setItem("token", token);
        router.push("/");
      }else{
        setLoading(false);
        message.error('No tienes autorización');
      }
    } catch (e) {
      message.error('Ingresa de nuevo tus credenciales');
      setLoading(false);
    }
  };

  return (
    <>
      <Auth>
        <Head />
        <main>
          <div className="container mx-auto px-4 h-full">
            <div className="flex content-center items-center justify-center h-full">
              <div className="w-full lg:w-4/12 px-4 ">
                <div className="relative flex flex-col min-w-0 break-words w-full mb-6 shadow-lg rounded-lg bg-blueGray-200 border-0">
                  <div className="rounded-t mb-0 px-6 py-6">
                    <div className="text-center mb-3 flex content-center items-center justify-center">
                      <img
                        src={logo}
                        className="content-center"
                        width="50%"
                        style={{ margin: "0, auto" }}
                      />
                    </div>
                    <hr className="mt-6 border-b-1 border-blueGray-300" />
                  </div>
                  <div className="flex-auto px-4 lg:px-10 py-10 pt-0">
                    <div className="text-blueGray-600 text-center mb-3 font-bold">
                      <small>Inicia sesión con tus credenciales</small>
                    </div>
                    <form onSubmit={handleSubmit(onSubmit)}>
                      <div className="relative w-full mb-3">
                        <label
                          className="block uppercase text-blueGray-600 text-xs font-bold mb-2"
                          htmlFor="grid-password"
                        >
                          Correo
                        </label>
                        <input
                          type="email"
                          className="border-0 px-3 py-3 placeholder-blueGray-300 text-blueGray-600 bg-white rounded text-sm shadow focus:outline-none focus:ring w-full ease-linear transition-all duration-150"
                          placeholder="Correo"
                          {...register("email", { required: true })}
                        />
                        {errors.email && (
                          <span style={{ color: "red" }}>
                            El correo es obligatorio*
                          </span>
                        )}
                      </div>

                      <div className="relative w-full mb-3">
                        <label
                          className="block uppercase text-blueGray-600 text-xs font-bold mb-2"
                          htmlFor="grid-password"
                        >
                          Contraseña
                        </label>
                        <input
                          type="password"
                          className="border-0 px-3 py-3 placeholder-blueGray-300 text-blueGray-600 bg-white rounded text-sm shadow focus:outline-none focus:ring w-full ease-linear transition-all duration-150"
                          placeholder="Contraseña"
                          {...register("password", { required: true })}
                        />
                        {errors.password && (
                          <span style={{ color: "red" }}>
                            El campo es requerido*
                          </span>
                        )}
                      </div>
                      <div>
                        <label className="inline-flex items-center cursor-pointer">
                          <input
                            id="customCheckLogin"
                            type="checkbox"
                            className="form-checkbox border-0 rounded text-blueGray-700 ml-1 w-5 h-5 ease-linear transition-all duration-150"
                          />
                          <span className="ml-2 text-sm font-semibold text-blueGray-600">
                            Recordarme
                          </span>
                        </label>
                      </div>

                      <div className="text-center mt-6">
                        {loading ? <BoxLoading color="#23AFDC" /> : <div></div>}

                        <button
                          className="bg-blueGray-800 text-white active:bg-blueGray-600 text-sm font-bold uppercase px-6 py-3 rounded shadow hover:shadow-lg outline-none focus:outline-none mr-1 mb-1 w-full ease-linear transition-all duration-150"
                          type="submit"
                          disabled={loading}
                        >
                          Iniciar sesión
                        </button>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </main>
      </Auth>
    </>
  );
}
