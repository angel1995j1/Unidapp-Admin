import React from "react";
import withAuth from "@helpers/private_route";
import Sidebar from "@components/Sidebar";
import Head from "@components/Head";
import { Layout } from "antd";
import MinutasTable from "@components/Minutas";

const Minutas = () => {
  const { Content } = Layout;
  return (
    <>
      <Layout>
        <Head />
        <Layout>
          <Sidebar>
            <Content>
              <MinutasTable />
            </Content>
          </Sidebar>
        </Layout>
      </Layout>
    </>
  );
};

export default withAuth(Minutas);
