import React from "react";
import withAuth from "@helpers/private_route";
import Sidebar from "@components/Sidebar";
import Head from "@components/Head";
import { Layout } from "antd";
import FormTask from "@components/FormTarea";

const Home = () => {
  const { Content } = Layout;

  return (
    <>
      <Layout>
        <Head />
        <Layout>
          <Sidebar>
            <Content>
              <FormTask />
            </Content>
          </Sidebar>
        </Layout>
      </Layout>
    </>
  );
}

export default withAuth(Home);