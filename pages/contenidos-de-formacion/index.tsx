import React from "react";
import withAuth from "@helpers/private_route";
import Sidebar from "@components/Sidebar";
import Head from "@components/Head";
import { Layout } from "antd";
import FormContenidos from '@components/Contenidos-Formacion';

const ContenidoFormacion = () => {
  const { Content } = Layout;
  return (
    <>
      <Layout>
        <Head />
        <Layout>
          <Sidebar>
            <Content>
              <FormContenidos />
            </Content>
          </Sidebar>
        </Layout>
      </Layout>
    </>
  );
};

export default withAuth(ContenidoFormacion);
