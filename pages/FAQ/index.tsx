import React from "react";
import withAuth from "@helpers/private_route";
import Sidebar from "@components/Sidebar";
import Head from "@components/Head";
import { Layout } from "antd";
import FAQ from '@components/FAQ';


const Faq = () => {
  const { Content } = Layout;

  return (
    <>
      <Layout>
        <Head />
        <Layout>
          <Sidebar>
            <Content>
              <FAQ />
            </Content>
          </Sidebar>
        </Layout>
      </Layout>
    </>
  );
};

export default withAuth(Faq);
