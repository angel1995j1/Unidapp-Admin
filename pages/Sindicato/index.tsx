import React, { useEffect, useState } from "react";
import axios from "axios";
import dynamic from "next/dynamic";

import withAuth from "@helpers/private_route";
import Sidebar from "@components/Sidebar";
import Head from "@components/Head";
import {
  Form,
  Button,
  Layout,
  Card,
  Typography,
  Col,
  message,
  Row,
} from "antd";
import QuillEditor from "@assets/style/editor.style";
import ReactHtmlParser from "react-html-parser";

const ReactQuill = dynamic(() => import("react-quill"), { ssr: false });
import "react-quill/dist/quill.snow.css";

const formats = [
  "header",
  "font",
  "size",
  "bold",
  "italic",
  "underline",
  "strike",
  "blockquote",
  "list",
  "bullet",
  "indent",
  "link",
  "image",
  "video",
];

const modules = {
  toolbar: [
    [{ header: "1" }, { header: "2" }, { font: [] }],
    [{ size: [] }],
    ["bold", "italic", "underline", "strike", "blockquote"],
    [
      { list: "ordered" },
      { list: "bullet" },
      { indent: "-1" },
      { indent: "+1" },
    ],
    ["link", "image", "video"],
    ["clean"],
  ],
  clipboard: {
    // toggle to add extra line breaks when pasting HTML:
    matchVisual: false,
  },
};

export default function TuSindicato(){
  const { Content } = Layout;
  const { Title } = Typography;

  const [form] = Form.useForm();
  const [value, setValue] = useState("");

  const [sindicato, setSindicato] = useState<any>();
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    loadData();
  }, []);

  const loadData = async () => {
    const token = localStorage.getItem("token");
    try {
      const response = await axios.get(
        "https://api-smc43.ondigitalocean.app/api/sindicato",
        {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        }
      );
      if (response.status === 200) {
        setSindicato(response.data[0]);
        setLoading(false);
      }
    } catch (e) {}
  };

  const onFinish = async (values: any) => {
    const token = localStorage.getItem("token");
    const response = await axios.put(
      `https://api-smc43.ondigitalocean.app/api/sindicato/${sindicato._id}`,
      {
        texto: value,
        created_by: "Administrador",
        updated_at: Date.now(),
      },
      {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      }
    );
    if (response.status === 200) {
      message.success("Se ha modificado correctamente tu sindicato");
      loadData();
    }
  };


  return (
    <>
      <Layout>
        <Head />

        <Sidebar>
          <Content>
            <Row>
              <Col span={12}>
                <Title>Tu sindicato</Title>

                <div className="text-sm text-gray-900">
                  {ReactHtmlParser(sindicato?.texto || "")}
                </div>
              </Col>
              <Col span={12} style={{ paddingLeft: "10px" }}>
                <Card loading={loading}>
                  <Form
                    style={{ textAlign: "center", alignContent: "center" }}
                    onFinish={onFinish}
                  >
                    <Form.Item name="texto">
                      <QuillEditor>
                        <ReactQuill
                          style={{
                            minHeight: "200px",
                            maxHeight: "250px",
                            height: "40vh",
                          }}
                          value={value}
                          defaultValue={sindicato}
                          formats={formats}
                          onChange={setValue}
                          modules={modules}
                          theme="snow"
                        />
                      </QuillEditor>
                    </Form.Item>
                    <Form.Item style={{ paddingTop: "8vh" }}>
                      <Button type="primary" htmlType="submit">
                        Guardar
                      </Button>
                    </Form.Item>
                  </Form>
                </Card>
              </Col>
            </Row>
          </Content>
        </Sidebar>
      </Layout>
    </>
  );
};

// export default withAuth(TuSindicato);
