import React from "react";
import withAuth from "@helpers/private_route";
import Sidebar from "@components/Sidebar";
import Head from "@components/Head";
import { Layout } from "antd";
import AsesoriasList from '@components/Asesorias';

const Asesorias = () => {
  const { Content } = Layout;
  return (
    <>
      <Layout>
        <Head />
        <Layout>
          <Sidebar>
            <Content>
              <AsesoriasList />
            </Content>
          </Sidebar>
        </Layout>
      </Layout>
    </>
  );
};

export default withAuth(Asesorias);
