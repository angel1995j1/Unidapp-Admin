import React from "react";
import "@fortawesome/fontawesome-free/css/all.min.css";
import "styles/tailwind.css";
import 'antd/dist/antd.css';
import SidebarContextWrapper from '../context/SidebarContext';

function MyApp({ Component, pageProps }:any) {
  return (
    <>
      <SidebarContextWrapper>
        <Component {...pageProps} />
      </SidebarContextWrapper>
    </>
  )
}

export default MyApp
