import React from 'react';
import withAuth from '@helpers/private_route';
import Sidebar from '@components/Sidebar';
import Head from '@components/Head';
import { Layout, Image, Typography } from 'antd';
import welcome from '@assets/img/welcome.png';

const Home = () => {
  const { Content } = Layout;
  const { Title } = Typography;

  return (
    <>
    <Layout>
      <Head />
      <Layout>
          <Sidebar>
            <Content style={{justifyContent: 'center', justifyItems: 'center', textAlign: 'center'}}>
              <Title >
                Bienvenido al Panel de control
              </Title>
              <Image
                src="error"
                fallback={welcome}
              />
            </Content>
          </Sidebar>
      </Layout>
    </Layout>
      
    </>
  );
}

export default withAuth(Home)
