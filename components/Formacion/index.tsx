import React, { Fragment, useEffect, useState } from "react";
import { Button } from 'antd';
import ReactHtmlParser from 'react-html-parser';
import Fade from "@material-ui/core/Fade";

import { BoxLoading } from "react-loadingg";
import { Grid } from "@material-ui/core";
import axios from "axios";

const Formacion = () => {
    const [state, setstate] = useState<any[]>([]);
    const [loading, setLoading] = useState(false);
    const [isModalVisible, setIsModalVisible] = useState(false);

    const showModal = () => {
      setIsModalVisible(true);
    };
  
    const handleOk = () => {
      setIsModalVisible(false);
    };
  
    const handleCancel = () => {
      setIsModalVisible(false);
    };

    useEffect(() => {
      const token = localStorage.getItem("token");
      setLoading(true);
      try {
        axios
          .get("https://api-smc43.ondigitalocean.app/api/contenidoFormacion", {
            headers: {
              Authorization: `Bearer ${token}`,
            },
          })
          .then((data: any) => {
            setstate(data.data.contenidos);
            // console.log("DATA", data);
            setLoading(false);
          });
      } catch (e) {
        console.error("ERROR AL OBTENER DATA", e);
        setLoading(false);
      }
    }, [])

    if (loading) {
        return (
          <div style={{ marginTop: "50vh" }}>
            <BoxLoading color="#23AFDC" />
          </div>
        );
      }


    return(
        <Fade in={!loading}>
        <Fragment>
          <Grid container spacing={3}>
            <Grid item xs={12}>
              <div className="shadow overflow-hidden border-b border-gray-200 sm:rounded-lg">
                <table className="w-full divide-y divide-gray-200">
                  <thead style={{ backgroundColor: "#1E293B" }}>
                    <tr>
                      <th
                        scope="col"
                        className="px-2 py-3 text-left text-xs font-medium text-white uppercase tracking-wider"
                      >
                        Titulo
                      </th>
                      <th
                        scope="col"
                        className="px-6 py-3 text-left text-xs font-medium text-white uppercase tracking-wider"
                      >
                        Detalle
                      </th>
                      <th scope="col" className="">
                        <span className="sr-only">Archivos</span>
                      </th>
                    </tr>
                  </thead>
                  <tbody className="bg-white divide-y divide-gray-200">
                    {state.map((task:any) => (
                      <tr key={task._id}>
                        <td className="px-6 py-4">
                          <div className="flex items-center">
                            <div className="ml-4">
                              <div className="text-sm font-medium text-gray-900">
                                {/* {task.users_permissions_users[0].email} */}
                                {task.titulo}
                              </div>
                              <div className="text-sm text-gray-500">
                                {/* {moment(task.published_at).locale("es-mx").calendar()} */}
                                {task.subtitulo}
                              </div> 
                            </div>
                          </div>
                        </td>
                        <td className="px-6 py-3">
                          <div className="text-sm text-gray-900">
                            {ReactHtmlParser(task.detalle)}
                            {/* {task.detalle} */}
                          </div>
                        </td>
                        <td className="px-6 py-4 whitespace-nowrap text-right text-sm font-medium">
                        <Button type="primary" onClick={showModal}>
                          Archivos
                        </Button>
                        </td>
                      </tr>
                    ))}
                  </tbody>
                </table>
              </div>
            </Grid>
          </Grid>
        </Fragment>
      </Fade>
    );
}

export default Formacion;