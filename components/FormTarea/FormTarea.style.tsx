import styled from "styled-components";

export const Column = styled.div`
  display: grid;
  grid-gap: 10px;
  grid-auto-rows: minmax(100px, auto);
`;

export const InputRow = styled.div`
    grid-column: 1 / 12;
    grid-row: 1
`;
export const ButtonRow = styled.div`
    grid-column: 12 / 12;
    grid-row: 1
`
