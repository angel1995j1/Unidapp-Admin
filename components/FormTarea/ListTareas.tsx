import React, { Fragment, useEffect, useState } from "react";
import axios from "axios";
import jsPDF from 'jspdf'

import Fade from "@material-ui/core/Fade";
import { Grid } from "@material-ui/core";
import { Table, Tag, Space, Popconfirm, Button, message } from "antd";
import { CloseCircleOutlined } from "@ant-design/icons";

const ListTarea = () => {
  const token = localStorage.getItem("token");
  const [state, setstate] = useState<any[]>([]);
  const [loading, setLoading] = useState(false);
  // const [open, setOpen] = useState(false);
  const columns = [
    {
      title: "Tarea",
      dataIndex: "tarea",
      key: "tarea",
    },
    {
      title: "Titulo",
      dataIndex: "titulo",
      key: "titulo",
    },
    {
      title: "Estado",
      key: "finalizado",
      dataIndex: "finalizado",
      render: (text: any) => {
        return (
          <Tag color={text ? "green" : "geekblue"}>
            {text ? "Finalizada" : "Activa"}
          </Tag>
        );
      },
    },
    {
      title: "Acción",
      key: "id",
      dataIndex: "_id",
      render: (record: any) => (
        <Space size="middle">
          <Popconfirm
            title="Estás seguro que deseas eliminar esta tarea?"
            cancelText="Cancelar"
            okText="Eliminar"
            onConfirm={() => deleteTask(record)}
            >
            <Button
              type="link"
              shape="circle"
              icon={<CloseCircleOutlined style={{ color: "#ff0000" }} />}
              size="small"
            ></Button>
          </Popconfirm>
        </Space>
      ),
    },
  ];
  
  useEffect(() => {
    setLoading(true);
    loadData();
  }, []);

  const loadData = async () => {
    try {
      const data = await axios.get("https://api-smc43.ondigitalocean.app/api/tareas", {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      });
      setstate(data.data.tareas);
      setLoading(false);
    } catch (e) {
      console.error("ERROR AL OBTENER DATA", e);
      setLoading(false);
    }
  };

  const deleteTask = async (id: any) => {
    setLoading(true)
    try{
      const response = await axios.delete(`https://api-smc43.ondigitalocean.app/api/eliminar-tarea/${id}`, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      });
      setLoading(false)
      if(response.status === 200){
        message.success('Se ha eliminado correctamente la tarea')
        loadData();
      }
    }catch(e){
      message.error('Algo salio mal, por favor intenta más tarde')
      setLoading(false)
    }
  }

  return (
    <Fade in={!loading}>
      <Fragment>
        <Grid container spacing={3}>
          <Grid item xs={12}>
            <div style={{ paddingTop: "20px" }}>
              <Table columns={columns} dataSource={state} loading={loading} rowKey={(data => data._id)}/>
            </div>
          </Grid>
        </Grid>
      </Fragment>
    </Fade>
  );
};

export default ListTarea;
