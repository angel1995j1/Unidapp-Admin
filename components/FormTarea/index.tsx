import React, { useState, Fragment, useEffect } from "react";
import { makeStyles, createStyles, Theme } from "@material-ui/core/styles";
import { useForm } from "react-hook-form";

import moment from 'moment';
import {
  Grid,
  Tabs,
  Tab,
  List,
  ListItem,
  ListItemAvatar,
  Avatar,
  ListItemText,
  ListItemSecondaryAction,
  IconButton,
} from "@material-ui/core";
import { DeleteOutline, Folder } from "@material-ui/icons";
import { BoxLoading } from "react-loadingg";

import {
  Form,
  Input,
  Button,
  Select,
  DatePicker,
  Card,
  Typography,
  Modal,
  Row, Col, message,
  Checkbox
} from "antd";



import ListTarea from "./ListTareas";
import axios from "axios";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      flexGrow: 1,
    },
    paper: {
      padding: theme.spacing(2),
      textAlign: "center",
      color: theme.palette.text.secondary,
    },
  })
);

const FormTask = () => {
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [ admin, setAdmin] = useState(false);
  const { register, handleSubmit } = useForm();

  const [value, setValue] = useState<any>([]);
  const [users, setUsers] = useState<any>([]);
  const [ form ] = Form.useForm();

  const [ loading, setLoading ] = useState(false);

  const [dataForm, setDataForm] = useState<any>();
  const [tab, setTab] = useState(0);

  const handleChangeTab = (event: React.ChangeEvent<{}>, newValue: number) => {
    setTab(newValue);
  };

  const { Title } = Typography;

  function onChangeCheckBox(e:any) {
    setAdmin(e.target.checked)
    // console.log(`checked = ${e.target.checked}`);
  }

  useEffect(() => {
    loadData();
  }, []);


  const loadData = async () => {
    setLoading(true)
    try{
      const token = localStorage.getItem("token");
      const response = await axios.get("https://api-smc43.ondigitalocean.app/api/usuarios", {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        });
        setUsers(response.data.usuarios);
        setLoading(false);
    }catch(e){
      setLoading(false)
      message.error('Algo salio mal')
    }
  }
  
  const setSubtaskFunction = async (e: any) => {
    // if (e.titulo != "") setValue((data: any) => [...data, e.titulo]);
    if (e.titulo != "") {
      let titulo = Object.assign({"titulo": e.titulo});
      setValue((data: any) => [...data, titulo]);
  }
    // console.log('INFORMACION para crear nuevas tareas', response);
  };

  const showModal = () => {
    setIsModalVisible(true);
  };

  const handleOk = () => {
    setIsModalVisible(false);
  };

  const handleCancel = () => {
    setIsModalVisible(false);
  };

  const removeSubtask = (index: any) => {
    const newSubtask = [...value];
    newSubtask.splice(index, 1);
    setValue(newSubtask);
  };


  const classes = useStyles();

  function onChange(value: any) {
    setDataForm({ ...dataForm, value });
  }

  function onSearch(val: any) {
    // console.log("search:", val);
  }

  const onFinish = async (values: any) => {
    setLoading(true);
    const token = localStorage.getItem("token");

    if(!admin){
      try{
        const response = await axios.post('https://api-smc43.ondigitalocean.app/api/crear-tarea', {
          "asignado": "Admin",
          "titulo": values.titulo,
          "tarea": values.tarea,
          "usuario": values.usuario,
          "creado": moment().format('MMMM Do YYYY, h:mm:ss a'),
          "fechaLimite": moment(values.fechaLimite).format('MMMM Do YYYY, h:mm:ss a'),
          "subtarea": value
      }, {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        });
        if(response.status===200)
        setLoading(false)
        form.resetFields();
        setValue([])
        message.success('Se ha creado correctamente la tarea');
      }catch(e){
        message.error('Ha ocurrido un error, intenta de nuevo más tarde')
        setLoading(false)
        // console.log('error--->', e)
      }
    }else{
      // console.log('enviar solo al administrador')
      try{
        const response = await axios.post('https://api-smc43.ondigitalocean.app/api/crear-tarea-directivos', {
          "asignado": "Admin",
          "titulo": values.titulo,
          "tarea": values.tarea,
          "creado": moment().format('MMMM Do YYYY, h:mm:ss a'),
          "fechaLimite": moment(values.fechaLimite).format('MMMM Do YYYY, h:mm:ss a'),
          "subtarea": value
      }, {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        });
        if(response.status===200)
        setLoading(false)
        form.resetFields();
        setValue([])
        message.success('Se ha creado correctamente la tarea');
      }catch(e){
        message.error('Ha ocurrido un error, intenta de nuevo más tarde')
        setLoading(false)
        //
      }
    }

  }

  const onFinishFailed = (errorInfo: any) => {
    // console.log('Failed:', errorInfo);
  };

  const layout = {
    labelCol: { span: 5 },
    wrapperCol: { span: 24 },
  };

  return (
    <>
      <div className={classes.root}>
        <Grid container>
          <Grid item xs={12}>
            <Tabs
              value={tab}
              indicatorColor="primary"
              onChange={handleChangeTab}
              aria-label="disabled tabs example"
              scrollButtons="on"
              centered={true}
              variant="fullWidth"
            >
              <Tab label="Crear Tarea" style={{ color: "#0f3d72" }} />
              <Tab label="Tareas activas" style={{ color: "#0f3d72" }} />
            </Tabs>
          </Grid>

          {tab == 0 ? (
            <Fragment>
              <Card.Grid
                style={{
                  paddingTop: "10vh",
                  width: "100%",
                  alignContent: "center",
                }}
              >
                <Title level={3} style={{ paddingBottom: "10px" }}>
                  Crea una tarea
                </Title>
                <Form {...layout} name="nest-messages"
                layout="horizontal"
                size="large"
                onFinish={onFinish}
                form={form}
                onFinishFailed={onFinishFailed}>
                  <Form.Item
                    name='usuario'
                    label="Usuarios"
                  >
                  <Select
                      showSearch
                      style={{ width: "100%" }}
                      placeholder="Selecciona uno o más usuarios"
                      optionFilterProp="children"
                      onChange={onChange}
                      onSearch={onSearch}
                      mode="multiple"
                      disabled={admin}
                    >
                      {users.map((user: any) => (
                        <option key={user._id} value={user.email}>
                          {user.email}
                        </option>
                      ))}
                      ;
                    </Select>
                  </Form.Item>
                  <Form.Item
                    name="titulo"
                    label="Titulo"
                    rules={[{ required: true }]}
                  >
                    <Input placeholder="Titulo de la tarea"/>
                  </Form.Item>
                  <Form.Item
                    name="fechaLimite"
                    label="Fecha limite"
                    rules={[{ required: true }]}
                  >
                    <DatePicker
                      showTime
                      format="YYYY-MM-DD HH:mm:ss"
                      placeholder="Agrega la fecha limite de la tarea"
                      style={{ width: "100%" }}
                    />
                  </Form.Item>
                  <Form.Item
                    name="tarea"
                    label="Tarea"
                  >
                    <Input.TextArea placeholder="Detalle de la tarea"/>
                  </Form.Item>

                  <Form.Item label="Subtareas" extra={`Se han agregado ${value.length} subtareas`}>
                    <Button onClick={showModal}>
                      Agrega Subtareas
                    </Button>
                  </Form.Item>

                  <Form.Item label="Enviar solo a Directivos">
                  <Checkbox
                    checked={admin}
                    onChange={onChangeCheckBox}
                  >
                  </Checkbox>
                  </Form.Item>

                  <Form.Item wrapperCol={{ ...layout.wrapperCol, offset: 8 }}>
                    <Button type="primary" htmlType="submit" loading={loading}>
                      Agregar Tarea
                    </Button>
                  </Form.Item>
                </Form>

                <Modal title="Agrega subtareas" visible={isModalVisible} onOk={handleOk} onCancel={handleCancel} width={800}>
                     <form onSubmit={handleSubmit(setSubtaskFunction)}>
                      <Form.Item label="Subtareas" extra="Es necesario dar click al botón de Agregar Subtarea para guardar los cambios.">
                        <Row gutter={8}>
                          <Col span={12}>
                            <Form.Item
                              noStyle
                              rules={[{ required: true, message: 'Por favor agrega subtareas!' }]}
                            >
                              <Input {...register("titulo")} />
                            </Form.Item>
                          </Col>
                          <Col span={12}>
                            <Button htmlType="submit">Agregar</Button>
                          </Col>
                        </Row>
                      </Form.Item>
                      <List dense={true}>
                         {value.map((res: any) => {
                            return (
                              <ListItem key={res._id}>
                                <ListItemAvatar>
                                  <Avatar>
                                    <Folder />
                                  </Avatar>
                                </ListItemAvatar>
                                <ListItemText secondary={res.titulo} />
                                <ListItemSecondaryAction>
                                  <IconButton
                                    edge="end"
                                    style={{ color: "#ff0000" }}
                                    aria-label="delete"
                                    onClick={removeSubtask}
                                  >
                                    <DeleteOutline />
                                  </IconButton>
                                </ListItemSecondaryAction>
                              </ListItem>
                            );
                          })}
                        </List>
                    </form>
                  </Modal>

              </Card.Grid>
            </Fragment>
          ) : (
            <ListTarea />
          )}
        </Grid>
      </div>
    </>
  );
};

export default FormTask;
