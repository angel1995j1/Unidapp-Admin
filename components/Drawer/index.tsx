import {
  Button,
  Image,
  message,
  Drawer,
  Form,
  Col,
  Row,
  Input,
  Select,
} from "antd";
import axios from "axios";
import { useState } from "react";
import { useForm } from "react-hook-form";

const DrawerComponent = (props: any) => {
  const { Option } = Select;
  const [loading, setLoading] = useState(false);
  const {
    register,
    handleSubmit,
    watch,
    formState: { errors },
  } = useForm();
  const onSubmit = (data: any) => {
    // console.log("onsubmit form", data);
  };

  async function activeAfiliacion(e: any) {
    const token = localStorage.getItem("token");
    setLoading(true);
    // console.log('props', props)

    if (e === "afiliar") {
      try {
        await axios.put(
          `https://api-smc43.ondigitalocean.app/api/usuario-afiliado/${props.props.id}`,
          {
            email: props.props.email,
            afiliado: true,
          },
          {
            headers: {
              Authorization: `Bearer ${token}`,
            },
          }
        );
        message.success(`Se a afiliado al usuario ${props.props.nombre}`);
        setLoading(false);
      } catch (e) {
        console.error("ERROR AL OBTENER DATA", e);
        setLoading(false);
      }
    } else {
      try {
        await axios.put(
          `https://api-smc43.ondigitalocean.app/api/usuario-afiliado/${props.props.id}`,
          {
            email: props.props.email,
            afiliado: false,
          },
          {
            headers: {
              Authorization: `Bearer ${token}`,
            },
          }
        );
        message.error(`Se a rechazado la afiliación a ${props.props.nombre}`);
        setLoading(false);
      } catch (e) {
        console.error("ERROR AL OBTENER DATA", e);
        setLoading(false);
      }
    }
  }

  async function onFinish(e: any) {
    const token = localStorage.getItem("token");
    setLoading(true);
    try {
    await axios.put(
        `https://api-smc43.ondigitalocean.app/api/actualizar-afiliacion/${props.props.id}`,
        e,
        {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        }
      );
      message.success(`Se a actualizado la afiliación ${props.props.nombre}`);
      setLoading(false);
    } catch (e) {
      console.error("ERROR AL OBTENER DATA", e);
      setLoading(false);
    }
  }

  return (
    <Drawer
      title="Información de la afiliación"
      width={720}
      visible={props.isVisible}
      onClose={props.onClick}
      bodyStyle={{ paddingBottom: 80 }}
    >
      <Form
        layout="vertical"
        hideRequiredMark
        key={props.props._id}
        onFinish={onFinish}
      >
        <Row gutter={16}>
          <Row gutter={16} justify="center" align="middle">
            <Col span={24}>
              <Form.Item label="Firma">
                <Image
                  src={`data:image/png;base64,${props.props.firmaAfiliacion}`}
                  width={150}
                  title="Ver"
                  placeholder="Ver"
                />
              </Form.Item>
            </Col>
          </Row>
          <Col span={12}>
            <Form.Item></Form.Item>
          </Col>
          <Col span={12}>
            <Form.Item
              name="correo"
              label="Correo"
              initialValue={props.props.email}
              // rules={[{ required: true, message: 'Please enter user name' }]}
            >
              <Input placeholder="Correo" id="correo" />
            </Form.Item>
          </Col>
          <Col span={12}>
            <Form.Item
              name="nombre"
              label="Nombre"
              initialValue={props.props.nombre}

              // rules={[{ required: true, message: 'Please enter user name' }]}
            >
              <Input placeholder="Nombre de usuario" id="nombre" />
            </Form.Item>
          </Col>
        </Row>
        <Row gutter={16}>
          <Col span={12}>
            <Form.Item
              name="tipoDocumento"
              label="Tipo de documento"
              initialValue={props.props.tipoDocumento}
            >
              <Input
                placeholder="Tipo de documento"
                id={props.props.tipoDocumento}
              />
            </Form.Item>
          </Col>
          <Col span={12}>
            <Form.Item
              name="numeroDocumento"
              label="Número de documento"
              initialValue={props.props.numeroDocumento}
            >
              <Input
                placeholder="Número de documento"
                id={props.props.numeroDocumento}
              />
            </Form.Item>
          </Col>
        </Row>
        <Row gutter={16}>
          <Col span={12}>
            <Form.Item
              name="ciudad"
              label="Ciudad"
              initialValue={props.props.ciudad}
            >
              <Input placeholder="Ciudad" id={props.props.ciudad} />
            </Form.Item>
          </Col>
          <Col span={12}>
            <Form.Item
              name="direccion"
              label="Dirección"
              initialValue={props.props.direccion}
            >
              <Input placeholder="Dirección" id={props.props.direccion} />
            </Form.Item>
          </Col>
        </Row>
        <Row gutter={16}>
          <Col span={12}>
            <Form.Item
              name="telefonoFijo"
              label="Teléfono fijo"
              initialValue={props.props.telefono_fijo}
            >
              <Input
                placeholder="Ingresa el telefono fijo"
                id={props.props.telefono_fijo}
              />
            </Form.Item>
          </Col>
          <Col span={12}>
            <Form.Item
              name="telefonoMovil"
              label="Teléfono Móvil"
              initialValue={props.props.telefono_movil}
            >
              <Input
                placeholder="Ingresa el teléfono móvil"
                id={props.props.telefono_movil}
              />
            </Form.Item>
          </Col>
        </Row>
        <Row gutter={16}>
          <Col span={12}>
            <Form.Item name="edad" label="Edad" initialValue={props.props.edad}>
              <Input placeholder="Ingresa la edad" id={props.props.edad} />
            </Form.Item>
          </Col>

          <Col span={12}>
            <Form.Item label="Selecciona una opción">
              <Select
                placeholder="Afiliar"
                allowClear
                onChange={(e) => activeAfiliacion(e)}
              >
                <Option value="afiliar">Afiliar</Option>
                <Option value="rechazar">Rechazar</Option>
              </Select>
            </Form.Item>
          </Col>
          <Col span={24}>
            <Form.Item>
              <Button type="primary" htmlType="submit" loading={loading}>
                Actualizar
              </Button>
              <Button style={{ marginLeft: 20 }} onClick={props.onClick}>
                Cancelar
              </Button>
            </Form.Item>
          </Col>
        </Row>
      </Form>
    </Drawer>
  );
};

export default DrawerComponent;
