import { Fragment, useState } from "react";
import axios from 'axios';
import { Grid } from "@material-ui/core";
import { Card, Button, Form, Input, Typography, message } from "antd";


const FormFAQ = () => {
    const { Title } = Typography;
    const { TextArea } = Input;
    const [loading, setLoading] = useState(false);

    const onFinish = async (values: any) => {
      setLoading(true);
      try{
        const token = localStorage.getItem("token");
        const response = await axios.post('https://api-smc43.ondigitalocean.app/api/preguntasFrecuentes', {
          "titulo": values.titulo,
          "detalles": values.detalles,
        }, {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        });
  
        if(response.status === 201){
          message.success('Se ha creado la pregunta')
          setLoading(false);
        }      
      }catch(e){
        message.error('Algo salio mal, intentalo de nuevo más tarde')
        // console.log('error', e);
      }
    }

    const onFinishFailed = (errorInfo: any) => {
      // console.log('Failed:', errorInfo);
    };

    return(
        <Fragment>
            <br></br>
            <Grid item xs={12} md={12}>
                <Card.Grid
                style={{
                    width: "100%",
                    textAlign: "center",
                    backgroundColor: "#ffff",
                    paddingTop: "40px",
                  }}
                >
                <Title level={3}>Crea preguntas frecuentes</Title>
                <Form
                  labelCol={{ span: 4 }}
                  wrapperCol={{ span: 14 }}
                  layout="horizontal"
                  size="large"
                  onFinish={onFinish}
                  onFinishFailed={onFinishFailed}
                >
                  <Form.Item label="Título" name="titulo">
                    <Input placeholder="Ingresa el titulo" />
                  </Form.Item>
                  <Form.Item label="Detalle" name="detalles">
                    <TextArea rows={5} placeholder="Ingresa el detalle de la respuesta" />
                  </Form.Item>

                  <Button size="large" htmlType="submit" loading={loading}>
                    Guardar
                  </Button>
                </Form>
                </Card.Grid>
            </Grid>
        </Fragment>
    );
}

export default FormFAQ;