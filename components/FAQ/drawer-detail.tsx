import { useState } from 'react';
import { Form, Input, Button, Drawer, message } from "antd";
import axios from "axios";


const DrawerFAQ = (props:any) => {
  const [loading, setLoading] = useState(false);

  const onFinish = async (values: any) => {
    setLoading(true);
    const token = localStorage.getItem("token");
    try{
      const response = await axios.put(`https://api-smc43.ondigitalocean.app/api/preguntasFrecuentes/${props.props._id}`, values, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      });
      if(response.status === 200){
        console.log('respuesta al modificar', response);
        message.success(response.data.msg);
        setLoading(false);
        props.onLoad()
      }
    }catch(e){
      // console.log('ERRRRORRRR--->>', e)
        setLoading(false);
        // message.error('Error al modificar la pregunta frecuente')
      // console.log('error --->>', e)
    }
  };
    const layout = {
        labelCol: { span: 5 },
        wrapperCol: { span: 16 },
      };

    return(
        <Drawer
            title="Información de la pregunta"
            width={720}
            visible={props.isVisible}
            onClose={props.onClick}
            bodyStyle={{ paddingBottom: 80 }} 
        >
      <Form {...layout} name="nest-messages" onFinish={onFinish}>
        <Form.Item
          name="titulo"
          label="Pregunta frecuente"
          rules={[{ required: true }]}
          initialValue={props.props.titulo}
        >
          <Input />
        </Form.Item>
        <Form.Item
          name="detalles"
          label="Detalles"
          initialValue={props.props.detalles}
        >
          <Input.TextArea placeholder="Ingresa la respuesta" rows={8}/>
        </Form.Item>
        <Form.Item wrapperCol={{ ...layout.wrapperCol, offset: 5 }} style={{paddingTop: '10vh'}}>
          <Button type="primary" htmlType="submit" loading={loading}>
            Guardar
          </Button>
        </Form.Item>
      </Form>
        </Drawer>
    );
}

export default DrawerFAQ;