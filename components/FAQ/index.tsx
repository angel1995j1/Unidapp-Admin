import React, { Fragment, useEffect, useState } from "react";
import axios from "axios";
import Fade from "@material-ui/core/Fade";
import DrawerFAQ from '@components/FAQ/drawer-detail';
import { BoxLoading } from "react-loadingg";
import { Grid, Tabs, Tab } from "@material-ui/core";
import { Button, Popconfirm, message } from "antd";
import { CloseCircleOutlined, EyeFilled } from "@ant-design/icons";
import FormFAQ from '@components/FAQ/form-faq';

const FaqList = () => {
  const [tab, setTab] = useState(0);
  const [state, setstate] = useState<any[]>([]);
  const [loading, setLoading] = useState(false);
  const [formVisibile, setFormVisible] = useState(false);
  const [dataDrawer, setData] = useState<any>();

  const [open, setOpen] = useState(false);

  const handleChangeTab = (event: React.ChangeEvent<{}>, newValue: number) => {
    setTab(newValue);
  };

  function viewContenido(data: any) {
    setFormVisible(true);
    setData(data);
  }

  function onCancelView() {
    setFormVisible(false);
  }

  useEffect(() => {
    loadData();
  }, []);


  function loadData(){
    const token = localStorage.getItem("token");
    setLoading(true);
    try {
      axios
        .get("https://api-smc43.ondigitalocean.app/api/preguntasFrecuentes", {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        })
        .then((data: any) => {
          setstate(data.data.preguntas);
          // console.log("DATA", data);
          setLoading(false);
        });
    } catch (e) {
      console.error("ERROR AL OBTENER DATA", e);
      setLoading(false);
    }
  }
  async function onDelete(data:any){
    setLoading(true);
    try{
      const token = localStorage.getItem("token");
      const response = await axios.delete(`https://api-smc43.ondigitalocean.app/api/preguntasFrecuentes/${data._id}`, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      });

      if(response.status === 200){
        message.success('Se ha eliminado correctamente')
        setLoading(false);
        loadData();
      }      
    }catch(e){
      message.error('Algo salio mal, intentalo de nuevo más tarde')
      // console.log('error', e);
    }
  }

  if (loading) {
    return (
      <div style={{ marginTop: "50vh" }}>
        <BoxLoading color="#23AFDC" />
      </div>
    );
  }

  return (
    <Fade in={!loading}>
      <Fragment>
        <Grid container spacing={3}>
          <Grid item xs={12}>
            <Tabs
              value={tab}
              indicatorColor="primary"
              onChange={handleChangeTab}
              aria-label="disabled tabs example"
              scrollButtons="on"
              centered={true}
              variant="fullWidth"
            >
              <Tab
                label="Preguntas frecuentes activas"
                style={{ color: "#0f3d72" }}
              />
              <Tab
                label="Crear Preguntas frecuentes"
                style={{ color: "#0f3d72" }}
              />
            </Tabs>
          </Grid>
        </Grid>
        {tab === 0 ? (
          <Grid container spacing={3}>
            <Grid item xs={12}>
            {formVisibile ? (
                <div key={dataDrawer._id}>
                  <DrawerFAQ
                    props={dataDrawer}
                    isVisible={formVisibile}
                    onClick={onCancelView}
                  />
                </div>
              ) : null}
              <div className="shadow overflow-hidden border-b border-gray-200 sm:rounded-lg">
                <table className="w-full divide-y divide-gray-200">
                  <thead style={{ backgroundColor: "#1E293B" }}>
                    <tr>
                      <th
                        scope="col"
                        className="px-2 py-3 text-left text-xs font-medium text-white uppercase tracking-wider"
                      >
                        Tarea
                      </th>
                      <th
                        scope="col"
                        className="px-6 py-3 text-left text-xs font-medium text-white uppercase tracking-wider"
                      >
                        Detalle
                      </th>
                      <th scope="col" className="relative px-6 py-3">
                        <span className="sr-only">Editar</span>
                      </th>
                    </tr>
                  </thead>
                  <tbody className="bg-white divide-y divide-gray-200">
                    {state.map((task: any) => (
                      <tr key={task._id}>
                        <td className="px-6 py-4">
                          <div className="flex items-center">
                            <div className="ml-4">
                              <div className="text-sm font-medium text-gray-900">
                                {task.titulo}
                              </div>
                              {/* <div className="text-sm text-gray-500">
                                      {moment(task.published_at).locale("es-mx").calendar()}
                                    </div> */}
                            </div>
                          </div>
                        </td>
                        <td className="px-6 py-3">
                          <div className="text-sm text-gray-900">
                            {task.detalles}
                          </div>
                        </td>
                        <td className="px-6 py-4 whitespace-nowrap text-right text-sm font-medium">
                        <Button
                              type="link"
                              shape="circle"
                              icon={<EyeFilled />}
                              onClick={() => viewContenido(task)}
                              size="small"
                            ></Button>

                            <Popconfirm
                              title="Estás seguro que deseas eliminar este contenido?"
                              cancelText="Cancelar"
                              okText="Eliminar"
                              onConfirm={() => onDelete(task)}
                            >
                              <Button
                                type="link"
                                shape="circle"
                                icon={
                                  <CloseCircleOutlined
                                    style={{ color: "#ff0000" }}
                                  />
                                }
                                size="small"
                              ></Button>
                            </Popconfirm>
                        </td>
                      </tr>
                    ))}
                  </tbody>
                </table>
              </div>
            </Grid>
          </Grid>
        ) : (
          <FormFAQ />
        )}
      </Fragment>
    </Fade>
  );
};

export default FaqList;
