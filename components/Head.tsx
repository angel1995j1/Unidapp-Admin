import Head from 'next/head';

export default function HeadInfo(){
    return(
        <Head>
          <title>Panel Unidapp</title>
          <meta name="theme-color" content="#0d386c" />
          <meta name="description" content="Unidapp" />
          <meta
            name="keywords"
            content="Unidapp"
          />
          <link
            href="https://fonts.googleapis.com/css?family=Raleway:500,600&display=swap"
            rel="stylesheet"
          ></link>
          <link
            href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i"
            rel="stylesheet"
          />
        </Head>
    );
}