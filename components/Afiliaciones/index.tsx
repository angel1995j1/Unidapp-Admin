import React, { useState, useEffect } from "react";
import { Table, Space, Popconfirm, Button, message, Layout, Tabs, Input } from "antd";

import Highlighter from 'react-highlight-words';
import axios from "axios";

import { EyeOutlined } from "@ant-design/icons";
import Drawer from "@components/Drawer";
import { FolderOutlined } from "@material-ui/icons";
import { SearchOutlined, DownloadOutlined } from '@ant-design/icons';


export const getFullDate = (date: string): string => {
  const dateAndTime = date.split('T');

  return dateAndTime[0].split('-').reverse().join('-');
};

export default function Afiliaciones({ color }: any) {
  const [dataSource, setDataSource] = useState<any>();
  const [value, setValue] = useState('');
  const [searchedColumn, setSearchedColumn] = useState<String>('');

  const [archive, setArchive] = useState<any>();
  const [loading, setLoading] = useState(false);
  const [formVisibile, setFormVisible] = useState(false);
  const [dataDrawer, setData] = useState<any>();
  const { TabPane } = Tabs;
  
  const getColumnSearchProps = (dataIndex:any) => ({
    filterDropdown: ({ setSelectedKeys, selectedKeys, confirm, clearFilters }:any) => (
      <div style={{ padding: 8 }}>
        <Input
          ref={(node:any) => {
            setSearchedColumn(node);
          }}
          placeholder={`Buscar ${dataIndex}`}
          value={value}
          onChange={e => {
            const currentValue = e.target.value;
            setValue(currentValue);
            const filterData = dataSource.filter((entry:any) => entry.empresa.toLowerCase().includes(currentValue));
            setDataSource(filterData);
          }}
          onPressEnter={() => handleSearch(selectedKeys, confirm, dataIndex)}
          style={{ marginBottom: 8, display: 'block' }}
        />
        <Space>
          <Button
            type="primary"
            onClick={() => handleSearch(selectedKeys, confirm, dataIndex)}
            icon={<SearchOutlined />}
            size="small"
            style={{ width: 90 }}
          >
            Buscar
          </Button>
          <Button onClick={() => handleReset(clearFilters)} size="small" style={{ width: 90 }}>
            Limpiar
          </Button>
          <Button
            type="link"
            size="small"
            onClick={() => {
              confirm({ closeDropdown: false });
            }}
          >
            Filter
          </Button>
        </Space>
      </div>
    ),
    filterIcon: (filtered:any) => <SearchOutlined style={{ color: filtered ? '#1890ff' : undefined }} />,
    onFilter: (value:any, record:any) => record?.empresa ? record.empresa.toString().toLowerCase() : '',
    render: (text:any) => 
        <Highlighter
          highlightStyle={{ backgroundColor: '#ffc069', padding: 0 }}
          searchWords={[value]}
          autoEscape
          textToHighlight={text ? text.toString() : ''}
      />
  });

  const handleSearch = (selectedKeys:any, confirm:any, dataIndex:any) => {
    const filterData = dataSource.filter((entry:any) => entry.empresa.includes(value));
    // console.log('Filterdata', filterData);
    // setDataSource(filterData);
  };
8
  const handleReset = (clearFilters:any) => {
    setLoading(true);
    loadData();
    clearFilters();
  };

  const columns = [
    {
      title: "Correo",
      key: "email",
      dataIndex: "email",
    },
    {
      title: "Nombre",
      dataIndex: "nombre",
      key: "nombre",
    },
    {
      title: "Empresa",
      dataIndex: "empresa",
      key: "empresa",
      ...getColumnSearchProps('empresa'),
    },
    {
      title: "Fecha",
      dataIndex: "creado",
      key: "creado",
      render: ((date:string) => getFullDate(date) )
    },
    {
      title: "Telefono fijo",
      dataIndex: "telefono_fijo",
      key: "telefono_fijo",
    },
    {
      title: "Móvil",
      dataIndex: "telefono_movil",
      key: "telefono_movil",
    },
    {
      title: "Acciones",
      key: "id",
      dataIndex: "id",
      render: (text: any, record: any) => (
        <Space size="middle">
          <Popconfirm
            title="Estás seguro que deseas archivar esta afiliación?"
            cancelText="Cancelar"
            okText="Eliminar"
            onConfirm={() => archivarAfiliacion(record.id_afiliacion)}
          >
            <Button
              type="link"
              shape="circle"
              icon={<FolderOutlined style={{ color: "#696969" }} />}
              size="middle"
            ></Button>
          </Popconfirm>
          <Button
            type="link"
            shape="circle"
            icon={<EyeOutlined />}
            size="middle"
            onClick={() => viewAfiliacion(record)}
          ></Button>

          <Button
          type="link"
          shape="circle"
          size="middle"
          icon={<DownloadOutlined />}
          onClick={() => createPDF(record)}
          >
          </Button>
        </Space>
      ),
    },
  ];

  const columnsArchive = [
    {
      title: "Correo",
      key: "email",
      dataIndex: "email",
    },
    {
      title: "Nombre",
      dataIndex: "nombre",
      key: "nombre",
    },
    {
      title: "Empresa",
      dataIndex: "empresa",
      key: "empresa",
      ...getColumnSearchProps('empresa'),
    },
    {
      title: "Fecha",
      dataIndex: "creado",
      key: "creado",
      render: ((date:string) => getFullDate(date) )
    },
    {
      title: "Telefono fijo",
      dataIndex: "telefono_fijo",
      key: "telefono_fijo",
    },
    {
      title: "Móvil",
      dataIndex: "telefono_movil",
      key: "telefono_movil",
    },
    {
      title: "Acciones",
      key: "id",
      dataIndex: "id",
      render: (text: any, record: any) => (
        <Space size="middle">
          <Popconfirm
            title="Estás seguro que deseas dearchivar esta afiliación?"
            cancelText="Cancelar"
            okText="Eliminar"
            onConfirm={() => unArchive(record.id_afiliacion)}
          >
            <Button
              type="link"
              shape="circle"
              icon={<FolderOutlined style={{ color: "#696969" }} />}
              size="middle"
            ></Button>
          </Popconfirm>
          <Button
            type="link"
            shape="circle"
            icon={<EyeOutlined />}
            size="middle"
            onClick={() => viewAfiliacion(record)}
          ></Button>
        </Space>
      ),
    },
  ];

  useEffect(() => {
    setLoading(true);
    loadData();
  }, []);

  function viewAfiliacion(data: any) {
    // console.log('DAtos para ver', data)
    setFormVisible(true);
    setData(data);
  }

  function onCancelView() {
    setFormVisible(false);
  }

  function loadData() {
    try {
      const token = localStorage.getItem("token");
      axios
        .get("https://api-smc43.ondigitalocean.app/api/afiliacionesList", {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        })
        .then((response: any) => {

          const info = response.data.data;
          const res = [];
          const archivadas = [];
          for (const key in info) {
            if (Object.prototype.hasOwnProperty.call(info, key)) {
              const element = info[key];
              if (!element.archivada) {
                res.push(info[key]);
              }else if(element.archivada){
                archivadas.push(info[key]);
              }
            }
          }
          setArchive(archivadas);
          setDataSource(res);
          setLoading(false);
        });
    } catch (e) {
      setLoading(false);
    }
  }

  function callback(key: any) {
    // console.log(key);
  }

  async function archivarAfiliacion(id: any) {
    const token = localStorage.getItem("token");
    try {
      const response = await axios.put(
        `https://api-smc43.ondigitalocean.app/api/afiliaciones/${id}`,
        // `http://localhost:3000/api/afiliaciones/${id}`,
        {
          archivada: true,
        },
        {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        }
      );
      setLoading(true);
      loadData();
      message.success(response.data.msg);
    } catch (e) {
      message.error("Algo ha salido mal");
    }
  }

  async function updateAfiliacion(data:any){
    const token = localStorage.getItem("token");
    try {
      const response = await axios.put(
        `https://api-smc43.ondigitalocean.app/api/actualizar-afiliacion/${data._id}`,
        data,
        {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        }
      );
      setLoading(true);
      loadData();
      message.success(response.data.msg);
    } catch (e) {
      console.log('ERROR AL ACTUALIZAR LA AFILIACIÓN')
      message.error("Algo ha salido mal");
    }
  }

  async function unArchive(id: any){
    const token = localStorage.getItem("token");
    try {
      const response = await axios.put(
        `https://api-smc43.ondigitalocean.app/api/afiliaciones/${id}`,
        {
          archivada: false,
        },
        {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        }
      );
      setLoading(true);
      loadData();
      message.success(response.data.msg);
    } catch (e) {
      message.error("Algo ha salido mal");
    }
  }


  async function createPDF(data:any){
    // console.log('INFORMACIÓN QUE SE VA A MANDAR A IMPRIMIR EN EL ARCHIVO', data);
    const token = localStorage.getItem("token");
    try{
      const response = await axios.post('https://api-smc43.ondigitalocean.app/api/descarga-afiliacion', {
        "id": data.id,
        "nombre": data.nombre,
        "edad": data.edad,
        "correo": data.email,
        "telefono_fijo": data.telefono_fijo ? data.telefono_fijo : 'No se registró su teléfono fijo',
        "telefono_movil": data.telefono_movil ? data.telefono_movil : 'No se registró su telefono móvil',
        "numeroDocumento": data.numeroDocumento ? data.numeroDocumento : 'No hay datos del documento',
        "ciudad": data.ciudad ? data.ciudad : 'No hay datos de la ciudad',
        "empresa": data.empresa ? data.empresa : 'No hay datos de la empresa',
        "direccion": data.direccion ? data.direccion : 'no hay dirección registrada'
      },{
        headers: {
          Authorization: `Bearer ${token}`,
        },
      });
      // console.log('data.file')
      if(response.status === 200){
        return window.open(response.data.file);
      }else{
        return message.warn('Algo ha salido mal, por favor intentelo de nuevo más tarde')
      }
    }catch(e:any){
      // console.log('Creación de pdf error', e);
      return message.error('Error al descargar el archivo')
    }
  }

  return (
    <Layout>
      <Tabs defaultActiveKey="1" onChange={callback}>
        <TabPane tab="Activas" key="1">
          <div style={{ paddingTop: "20px" }}>
            {formVisibile ? (
              <div key={dataDrawer._id}>
                <Drawer
                  props={dataDrawer}
                  isVisible={formVisibile}
                  onClick={onCancelView}
                  Accept={updateAfiliacion}
                />
              </div>
            ) : null}
            <Table
              columns={columns}
              dataSource={dataSource}
              bordered={true}
              rowKey={(data) => data._id}
              loading={loading}
            />
          </div>
        </TabPane>
        <TabPane tab="Archivadas" key="2">
        <Table
              columns={columnsArchive}
              dataSource={archive}
              bordered={true}
              rowKey={(data) => data._id}
              loading={loading}
            />
        </TabPane>
      </Tabs>
    </Layout>
  );
}
