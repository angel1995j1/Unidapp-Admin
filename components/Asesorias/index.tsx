import React, { useEffect, useState } from "react";
import axios from "axios";
import { Table, Space, Popconfirm, Button, message, Layout, Tabs } from "antd";


import { CloudDownload, FolderOutlined } from "@material-ui/icons";

const AsesoriasList = () => {
  const [state, setstate] = useState<any[]>([]);
  const [archive, setArchive] = useState<any>();

  const [loading, setLoading] = useState(false);
  const { TabPane } = Tabs;

  function callback(key: any) {
    // console.log(key);
  }

  const columns = [
    {
      title: "Usuario",
      dataIndex: "usuario",
      key: "usuario",
    },
    {
      title: "descripcion",
      dataIndex: "descripcion",
      key: "descripcion",
    },
    {
      title: "Acciones",
      key: "file",
      dataIndex: ["file", "_id"],
      render: (text: any, record: any) => (
        <Space size="middle">
          <Popconfirm
            title="Estás seguro que deseas archivar esta asesoria?"
            cancelText="Cancelar"
            okText="Si"
            onConfirm={() => archiveSesoria(record._id)}
          >
            <Button
              type="link"
              shape="circle"
              icon={<FolderOutlined style={{ color: "#696969" }} />}
              size="middle"
            ></Button>
          </Popconfirm>
          <Button
            type="link"
            shape="circle"
            icon={<CloudDownload />}
            size="middle"
            onClick={() => downloadFiles(record.file)}
          ></Button>
        </Space>
      ),
    },
  ];

  const columnsArchive = [
    {
      title: "Usuario",
      dataIndex: "usuario",
      key: "usuario",
    },
    {
      title: "descripcion",
      dataIndex: "descripcion",
      key: "descripcion",
    },
    {
      title: "Acciones",
      key: "file",
      dataIndex: ["file", "_id"],
      render: (text: any, record: any) => (
        <Space size="middle">
          <Popconfirm
            title="Estás seguro que deseas desarchivar esta asesoria?"
            cancelText="Cancelar"
            okText="Si"
            onConfirm={() => unarchive(record._id)}
          >
            <Button
              type="link"
              shape="circle"
              icon={<FolderOutlined style={{ color: "#696969" }} />}
              size="middle"
            ></Button>
          </Popconfirm>
          <Button
            type="link"
            shape="circle"
            icon={<CloudDownload />}
            size="middle"
            onClick={() => downloadFiles(record.file)}
          ></Button>
        </Space>
      ),
    },
  ];

  useEffect(() => {
    loadData();
  }, []);

  const loadData = () => {
    const token = localStorage.getItem("token");
    setLoading(true);
    try {
      axios
        .get("https://api-smc43.ondigitalocean.app/api/asesorias", {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        })
        .then((data: any) => {
          const info = data.data.asesorias;
          const res = [];
          const archivadas = [];
          for (const key in info) {
            if (Object.prototype.hasOwnProperty.call(info, key)) {
              const element = info[key];
              if(!element.archivada){
                res.push(info[key]);
              }else if(element.archivada){
                archivadas.push(info[key]);
              }
            }
          }
          setArchive(archivadas);
          setstate(res);
          setLoading(false);
        });
    } catch (e) {
      console.error("ERROR AL OBTENER DATA", e);
      setLoading(false);
    }
  }

  const downloadFiles = (data: any) => {
    // console.log("DATA DE LOS FILES", data);
    if (data.length === 1) {
      window.open(data[0]);
    } else {
      for (const key in data) {
        if (Object.prototype.hasOwnProperty.call(data, key)) {
          const element = data[key];
          window.open(
            element,
            "_blank"
          );
        }
      }
    }
  };

  const archiveSesoria = async (data: any) => {
    setLoading(true);
    const token = localStorage.getItem("token");
    const response = await axios.put(
      `https://api-smc43.ondigitalocean.app/api/asesorias/${data}`,
      {
        archivada: true,
      },
      {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      }
    );
    if (response.status === 200) {
      message.success(response.data.msg);
      loadData();
    } else if (response.status === 404) {
      setLoading(false);
      message.error("Contacte a soporte por favor");
    }
  };

  const unarchive = async (data: any) => {
    setLoading(true);
    const token = localStorage.getItem("token");
    const response = await axios.put(
      `https://api-smc43.ondigitalocean.app/api/asesorias/${data}`,
      {
        archivada: false,
      },
      {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      }
    );
    if (response.status === 200) {
      message.success(response.data.msg);
      loadData();
    } else if (response.status === 404) {
      setLoading(false);
      message.error("Contacte a soporte por favor");
    }
  };

  return (
    <Layout>
       <Tabs defaultActiveKey="1" onChange={callback}>
       <TabPane tab="Activas" key="1">
       <div style={{ paddingTop: "20px" }}>
        <Table
          columns={columns}
          dataSource={state}
          bordered={true}
          rowKey={(data) => data._id}
          loading={loading}
        />
      </div>
       </TabPane>
       <TabPane tab="Archivadas" key="2">
        <Table
              columns={columnsArchive}
              dataSource={archive}
              bordered={true}
              rowKey={(data) => data._id}
              loading={loading}
            />
        </TabPane>
       </Tabs>
    </Layout>
  );
};

export default AsesoriasList;
