import { Form, Input, Button, Drawer, List, Typography, message } from "antd";
import axios from "axios";
import { useState } from "react";

const DrawerAsamblea = (props: any) => {
    const { Title } = Typography;
    const [ loading, setLoading ] = useState(false);


  const layout = {
    labelCol: { span: 5 },
    wrapperCol: { span: 16 },
  };

  const onFinish = async (e:any) => {

  }

  return (
    <Drawer
      title="Información de la minuta"
      width={720}
      visible={props.isVisible}
      onClose={props.onClick}
      bodyStyle={{ paddingBottom: 80 }}
    >
      <Form {...layout} name="nest-messages" style={{textAlign: 'center'}} onFinish={onFinish} method="POST">
        <Form.Item
          name="created_at"
          label="Creado"
          rules={[{ required: true }]}
          initialValue={props.props.created_at}
        >
          <Input />
        </Form.Item>
        <Form.Item
          name="created_by"
          label="Creada"
          initialValue={props.props.created_by}
        >
          <Input placeholder="Ingresa el número de acta" />
        </Form.Item>
        <Form.Item name="idAsamblea" label="ID Asamblea" initialValue={props.props.idAsamblea}>
          <Input />
        </Form.Item>
        <Title level={3}>Lista de usuarios</Title>
        <List
          itemLayout="horizontal"
          key={props.props.asistentes}
          dataSource={props.props.asistentes}
          renderItem={(item: any) => (
            <List.Item>
              <List.Item.Meta title={<a href="#">{item}</a>} />
            </List.Item>
          )}
        /> 
        
        <Form.Item
          wrapperCol={{ ...layout.wrapperCol, offset: 5 }}
          style={{ paddingTop: "10vh" }}
        >
          <Button type="primary" htmlType="submit" loading={loading}>
            Guardar
          </Button>
        </Form.Item>
      </Form>
    </Drawer>
  );
};

export default DrawerAsamblea;
