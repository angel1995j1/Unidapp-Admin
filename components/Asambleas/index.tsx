import React, { useEffect, useState } from "react";
import axios from "axios";
import { Table, Layout, Space, Button } from "antd";
import { FaEye } from "react-icons/fa";
import moment from "moment";
import DrawerAsamblea from "./drawer-detail";

const ListaAsambleas = () => {
  const [state, setstate] = useState<any[]>([]);
  const [loading, setLoading] = useState(false);
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [props, setProps] = useState<any>();

  const columns = [
    {
      title: "Fecha",
      dataIndex: "created_at",
      key: "created_at",
      render: (text:any) => <a>{moment(text).locale("es-mx").calendar()}</a>,
    },
    {
      title: "Creada",
      dataIndex: "created_by",
      key: "created_by",
    },
    {
      title: "ID Asamblea",
      dataIndex: "idAsamblea",
      key: "idAsamblea",
    },
    {
      title: "Acción",
      key: "id",
      dataIndex: "_id",
      render: (text: any, record: any) => (
        <Space size="middle">
          <Button
            onClick={() => viewContenido(record)}
            type="link"
            shape="circle"
            icon={<FaEye />}
            size="middle"
          ></Button>
        </Space>
      ),
    },
  ];

  useEffect(() => {
    loadData();
  }, []);

  const loadData = () => {
    const token = localStorage.getItem("token");
    setLoading(true);
    try {
      axios
        .get("https://api-smc43.ondigitalocean.app/api/asamblea", {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        })
        .then((data: any) => {
          setstate(data.data);
          setLoading(false);
        });
    } catch (e) {
      // console.log("ERROR AL OBTENER DATA", e);
    }
  };

  function viewContenido(data: any) {
    setIsModalVisible(true);
    setProps(data);
    // console.log('DATA', data);
  }

  function onCancelView() {
    setIsModalVisible(false);
  }

  return (
    <Layout>
      <div style={{ paddingTop: "20px" }}>
        {isModalVisible ? (
          <div key={props._id}>
            <DrawerAsamblea
              props={props}
              isVisible={isModalVisible}
              onClick={onCancelView}
            />
          </div>
        ) : null}
        <Table
          columns={columns}
          dataSource={state}
          bordered={true}
          rowKey={(data) => data._id}
          loading={loading}
        />
      </div>
    </Layout>
  );
};

export default ListaAsambleas;
