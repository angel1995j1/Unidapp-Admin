import React from 'react';
import { Link, useRouteMatch } from 'react-router-dom';

import { Menu } from 'antd';

const SubMenu = Menu.SubMenu;

const stripTrailingSlash = (str:any) => {
  if (str.substr(-1) === '/') {
    return str.substr(0, str.length - 1);
  }
  return str;
};
export default React.memo(function SidebarMenu({
  singleOption,
  submenuStyle,
  submenuColor,
  ...rest
}:any) {
  let match = useRouteMatch();

  const { key, label, leftIcon, children } = singleOption;
  const url = stripTrailingSlash(match.url);

  if (children) {
    return (
      <SubMenu
        key={key}
        title={
          <span className="isoMenuHolder" style={submenuColor}>
            {leftIcon}
            <span className="nav-text">
              {label}
            </span>
          </span>
        }
        {...rest}
      >
        {children.map((child:any) => {
          const linkTo = child.withoutDashboard
            ? `/${child.key}`
            : `${url}/${child.key}`;
          return (
            <Menu.Item style={submenuStyle} key={child.key}>
              <Link style={submenuColor} to={linkTo}>
                {child.label}
              </Link>
            </Menu.Item>
          );
        })}
      </SubMenu>
    );
  }

  return (
    <Menu.Item key={key} {...rest}>
      <Link to={`${url}/${key}`}>
        <span className="isoMenuHolder" style={submenuColor}>
          {leftIcon}
          <span className="nav-text">
            {/* <IntlMessages id={label} /> */}
            {label}
          </span>
        </span>
      </Link>
    </Menu.Item>
  );
});
