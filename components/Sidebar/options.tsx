import React from 'react';
import { MessageOutlined } from '@ant-design/icons';

const options = [
  {
    key: 'blankPage',
    label: 'sidebar.blankPage',
    leftIcon: <MessageOutlined size={19} />,
  },
  {
    key: 'authCheck',
    label: 'sidebar.authCheck',
    leftIcon: <MessageOutlined size={19} />,
  },
];
export default options;
