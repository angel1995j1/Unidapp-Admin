import React, { useState } from "react";
import Link from "next/link";
import { useRouter } from "next/router";
import { Layout, Menu, Breadcrumb, Button } from "antd";
import logo from "@assets/img/logo.png";
import { FaTasks, FaUsers, FaShapes, FaQuestion, FaFile, FaSignOutAlt, FaEraser, FaComments } from 'react-icons/fa';

import {
  PieChartOutlined,
} from "@ant-design/icons";

const { Header, Content, Sider } = Layout;

const Sidebar = ({ children }: any) => {
  const [collapse, setCollapse] = useState(false);
  const router = useRouter();

  const outSession = () => {
    localStorage.clear();
    router.push("/auth/login");
  };

  const onCollapse = (collapsed: boolean) => {
    setCollapse(collapsed);
  };

  return (
    <>
      <Layout style={{ minHeight: "100vh" }}>

        <Sider collapsible collapsed={collapse} onCollapse={onCollapse}>
          <div className="logo" style={{ margin: "20px" }}>
            <img src={logo} />
          </div>
          <Menu
            theme="dark"
            defaultSelectedKeys={[
              router.pathname === "/afiliaciones"
                ? "1"
                : router.pathname === "/tareas"
                ? "2"
                : router.pathname === "/minutas"
                ? "3"
                : router.pathname === "/asesorias"
                ? "4"
                : router.pathname === "/contenidos-de-formacion"
                ? "5"
                : router.pathname === "/preguntas-frecuentes"
                ? "6"
                : router.pathname === "/Sindicato"
                ? "7"
                : router.pathname === "/Asambleas"
                ? "8"
                : "1",
            ]}
            mode="inline"
          >
            <Menu.Item key="1" icon={<PieChartOutlined />}>
              <Link href="/afiliaciones">Afiliaciones</Link>
            </Menu.Item>
            <Menu.Item key="2" icon={<FaTasks />}>
              <Link href="/tareas">Tareas</Link>
            </Menu.Item>
            <Menu.Item key="3" icon={<FaEraser />}>
              <Link href="/minutas">Minutas</Link>
            </Menu.Item>
            <Menu.Item key="4" icon={<FaUsers />}>
              <Link href="/asesorias">Asesorias Juridicas</Link>
            </Menu.Item>
            <Menu.Item key="5" icon={<FaShapes />}>
              <Link href="/contenidos-de-formacion">
                Contenidos de formación
              </Link>
            </Menu.Item>
            <Menu.Item key="6" icon={<FaQuestion />}>
              <Link href="/FAQ">Preguntas frecuentes</Link>
            </Menu.Item>
            <Menu.Item key="7" icon={<FaFile />}>
              <Link href="/Sindicato">Sindicato</Link>
            </Menu.Item>
            <Menu.Item key="8" icon={<FaComments />}>
              <Link href="/Asambleas">Asambleas</Link>
            </Menu.Item>
            <Menu.Item key="9" icon={<FaSignOutAlt />}>
              <Button
                type="text"
                onClick={outSession}
                style={{ color: "#ffff" }}
              >
                Salir
              </Button>
            </Menu.Item>
          </Menu>
        </Sider>
        <Layout className="site-layout">
          <Header className="site-layout-background" style={{ padding: 0 }} />
          <Content style={{ margin: "0 16px" }}>
            <Breadcrumb style={{ margin: "16px 0" }}>
              <Breadcrumb.Item>Navegacion</Breadcrumb.Item>
              <Breadcrumb.Item>{router.pathname.split("/")}</Breadcrumb.Item>
            </Breadcrumb>
            <div
              className="site-layout-background"
              style={{ padding: 24, minHeight: 360 }}
            >
              {children}
            </div>
          </Content>
        </Layout>
      </Layout>
    </>
  );
};

export default Sidebar;
