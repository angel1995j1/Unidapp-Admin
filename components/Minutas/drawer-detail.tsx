import { Form, Input, Button, Drawer, List, Typography, message } from "antd";
import axios from "axios";
import { useState } from "react";

const DrawerMinuta = (props: any) => {
    const { Title } = Typography;
    const [ loading, setLoading ] = useState(false);


  const layout = {
    labelCol: { span: 5 },
    wrapperCol: { span: 16 },
  };

  const onFinish = async (e:any) => {
    const token = localStorage.getItem("token");
    setLoading(true);

    try{
        const response = await axios.put(`https://api-smc43.ondigitalocean.app/api/minuta/${props.props._id}`,
        {
          "actaNo": e.actaNo,
          "fecha": e.fecha,
          "idAsamblea": e.idAsamblea,
          "resumen": e.resument
        }, {
            headers: {
                Authorization: `Bearer ${token}`,
              },
        });
        if(response.status === 200){
            setLoading(false);
            message.success(response.data.msg);
        }
    }catch(e){
      // console.log('error', e)
        message.error('Algo salió mal')
        setLoading(false);
    }
  }

  return (
    <Drawer
      title="Información de la minuta"
      width={720}
      visible={props.isVisible}
      onClose={props.onClick}
      bodyStyle={{ paddingBottom: 80 }}
    >
      <Form {...layout} name="nest-messages" style={{textAlign: 'center'}} onFinish={onFinish} method="POST">
        <Form.Item
          name="tipoasamblea"
          label="Tipo Asamblea"
          rules={[{ required: true }]}
          initialValue={props.props.tipoAsamblea}
        >
          <Input />
        </Form.Item>
        <Form.Item
          name="actaNo"
          label="Noº Acta"
          initialValue={props.props.actaNo}
        >
          <Input placeholder="Ingresa el número de acta" />
        </Form.Item>
        <Form.Item name="fecha" label="Fecha" initialValue={props.props.fecha}>
          <Input />
        </Form.Item>
        <Form.Item name="idAsamblea" label="ID Asamblea" initialValue={props.props.idAsamblea}>
          <Input />
        </Form.Item>
        <Form.Item
          name="resumen"
          label="Resúmen"
          initialValue={props.props.resumen}
        >
          <Input.TextArea rows={4} />
        </Form.Item>
        <Title level={3}>Ordenes</Title>
        <List
          itemLayout="horizontal"
          key={props.props.ordenDia}
          dataSource={props.props.ordenDia}
          renderItem={(item: any) => (
            <List.Item>
              <List.Item.Meta title={<a href="#">{item}</a>} />
            </List.Item>
          )}
        /> 
        <Form.Item
          wrapperCol={{ ...layout.wrapperCol, offset: 5 }}
          style={{ paddingTop: "10vh" }}
        >
          <Button type="primary" htmlType="submit" loading={loading}>
            Guardar
          </Button>
        </Form.Item>
      </Form>
    </Drawer>
  );
};

export default DrawerMinuta;
