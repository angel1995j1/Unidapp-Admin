import React, { Fragment, useEffect, useState } from "react";
import axios from "axios";
import moment from "moment";
import Fade from "@material-ui/core/Fade";
import jsPDF from 'jspdf'
import autoTable from 'jspdf-autotable'

import { CloseCircleOutlined } from "@ant-design/icons";
import { FaEye, FaFileDownload } from 'react-icons/fa';
import { Button, message, Popconfirm, Space, Table } from "antd";
import { Grid } from "@material-ui/core";
import DrawerMinuta from "./drawer-detail";

const MinutasTable = () => {
  const [loading, setLoading] = useState(false);
  const [minutas, setMinutas] = useState<any>([]);
  const [props, setProps] = useState<any>();
  const [isModalVisible, setIsModalVisible] = useState(false);

  const columns = [
    {
      title: "Tipo Asamblea",
      dataIndex: "tipoAsamblea",
      key: "tipoAsamblea",
    },
    {
      title: "Noº Acta",
      dataIndex: "actaNo",
      key: "actaNo",
    },
    {
      title: "Fecha",
      key: "fecha",
      dataIndex: "fecha",
      render: (record: any) => (
        <p>{moment(record).locale("es-mx").calendar()}</p>
      )
    },
    {
      title: "Resúmen",
      key: "resumen",
      dataIndex: "resumen",
    },
    {
      title: "Acción",
      key: "id",
      dataIndex: "_id",
      render: (text: any, record: any) => (
        <Space size="middle">
          <Popconfirm
            title="Estás seguro que deseas eliminar esta minuta?"
            cancelText="Cancelar"
            okText="Eliminar"
            onConfirm={() => removeMinuta(record)}
            >
            <Button
              type="link"
              shape="circle"
              icon={<CloseCircleOutlined style={{ color: "#ff0000" }} />}
              size="middle"
            ></Button>
          </Popconfirm>
          <Button
              onClick={() => viewContenido(record)}
              type="link"
              shape="circle"
              icon={<FaEye />}
              size="middle"
          ></Button>
          <Button
              onClick={() => generatePDF(record)}
              type="link"
              shape="circle"
              icon={<FaFileDownload />}
              size="middle"
          ></Button>
        </Space>
      ),
    },
  ];

  useEffect(() => {
    setLoading(true);
    loadData();
  }, []);

  function loadData() {
    try {
      const token = localStorage.getItem("token");
      axios
        .get("https://api-smc43.ondigitalocean.app/api/minutas", {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        })
        .then((res: any) => {
          setMinutas(res.data.minutas);
          setLoading(false);
        });
    } catch (e) {
      setLoading(false);
    }
  }

  function viewContenido(data: any) {
    setIsModalVisible(true);
    setProps(data);
  }

  async function removeMinuta(id: any) {
    const token = localStorage.getItem("token");
    try {
      await axios.delete(
        `https://api-smc43.ondigitalocean.app/api/minuta/${id._id}`,
        {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        }
      );
      setLoading(true);
      loadData();
      message.success('Se ha eliminado correctamente');
    } catch (e) {
      // console.log("ERROR AL ELIMINAR AFILIACION--->>", e);
      message.error("Algo ha salido mal");
    }
  }

  function onCancelView() {
    setIsModalVisible(false);
  }


  const generatePDF = (minuta:any) => {

    const doc = new jsPDF()
    // autoTable(doc, { html: '#my-table' })
    autoTable(doc, {
      head: [['Acta No.', 'ID Asamblea', 'Resúmen', 'Usuario']],
      body: [
        [minuta.actaNo, minuta.idAsamblea, minuta.resumen, minuta.usuario],
      ],
    })
    doc.save(`minuta-${minuta.idAsamblea}.pdf`);
  }

  return (
    <Fade in={!loading}>
      <Fragment>
        <Grid container spacing={3}>
          <Grid item xs={12}>
          {isModalVisible ? (
                <div key={props._id}>
                  <DrawerMinuta
                    props={props}
                    isVisible={isModalVisible}
                    onClick={onCancelView}
                  />
                </div>
              ) : null}
            <div style={{ paddingTop: "20px" }}>
              <Table columns={columns} dataSource={minutas} rowKey={(data) => data._id} loading={loading}/>
            </div>
          </Grid>
        </Grid>
      </Fragment>
    </Fade>
  );
};

export default MinutasTable;
