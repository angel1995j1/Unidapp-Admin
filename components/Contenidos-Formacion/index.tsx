import React, { useState, Fragment, useEffect } from "react";
import axios from "axios";
import dynamic from "next/dynamic";

import { useForm } from "react-hook-form";
import { Grid, Tabs, Tab } from "@material-ui/core";
import { Form, Input, Button, Typography, Card, Popconfirm, message } from "antd";
import { CloseCircleOutlined, EyeFilled } from "@ant-design/icons";
import ReactHtmlParser from "react-html-parser";
const ReactQuill = dynamic(() => import("react-quill"), { ssr: false });
import "react-quill/dist/quill.snow.css";

import DrawerContenido from "@components/Contenidos-Formacion/detail";


const FormContenidos = () => {
  const { register, handleSubmit, watch, formState: { errors } } = useForm();
  const [tab, setTab] = useState(0);
  const [state, setstate] = useState<any[]>([]);
  const [formVisibile, setFormVisible] = useState(false);
  const [dataDrawer, setData] = useState<any>();

  const [loading, setLoading] = useState(false);

  const [editText, setEditText] = useState("");
  const { Title } = Typography;

  const handleChangeTab = (event: React.ChangeEvent<{}>, newValue: number) => {
    setTab(newValue);
  };

  useEffect(() => {
    setLoading(true);
    loadData();
  }, []);

  function loadData(){
    const token = localStorage.getItem("token");
    try {
      axios
        .get("https://api-smc43.ondigitalocean.app/api/contenidoFormacion", {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        })
        .then((data: any) => {
          // console.log("INFORMACION DE LA DATA", data);
          setstate(data.data.contenidos);
          setLoading(false);
        });
    } catch (e) {
      console.error("ERROR AL OBTENER DATA", e);
      setLoading(false);
    }
  }
  const modules = {
    toolbar: [
      [{ header: [1, 2, false] }],
      ["bold", "italic", "underline", "strike", "blockquote"],
      [
        { list: "ordered" },
        { list: "bullet" },
        { indent: "-1" },
        { indent: "+1" },
      ],
      ["link", "image"],
      ["clean"],
    ],
  };

  function viewContenido(data: any) {
    setFormVisible(true);
    setData(data);
  }

  function onCancelView() {
    setFormVisible(false);
  }
  async function onDelete(data:any) {
    const token = localStorage.getItem("token");
      try{
        const response = await axios.delete(`https://api-smc43.ondigitalocean.app/api/contenidoFormacion/${data._id}`, {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        })
        if(response.status === 200 ){
          message.success('Se ha eliminado correctamente el contenido')
          loadData();
        }
        // console.log('response to delete contenido ===>>', response);
      }catch(e){
        // console.log('error', e);
      }
  }

  const onFinish = async (values: any) => {
    setLoading(true);
    try{
      const token = localStorage.getItem("token");
      const response = await axios.post('https://api-smc43.ondigitalocean.app/api/crearContenido', {
        "titulo": values.titulo,
        "subtitulo": values.subtitulo,
        "video": values.video,
        "detalle": editText
      }, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      });

      if(response.status === 201){
        message.success('Se ha creado el contenido de formación')
        loadData();
      }      
    }catch(e){
      message.error('Algo salio mal, intentalo de nuevo más tarde')
      // console.log('error', e);
    }

  };

  const onFinishFailed = (errorInfo: any) => {
    // console.log('Failed:', errorInfo);
  };

  return (
    <>
      <Grid container spacing={3}>
        <Grid item xs={12}>
          <Tabs
            value={tab}
            indicatorColor="primary"
            onChange={handleChangeTab}
            aria-label="disabled tabs example"
            scrollButtons="on"
            centered={true}
            variant="fullWidth"
          >
            <Tab label="Crear Contenido" style={{ color: "#0f3d72" }} />
            <Tab
              label="Contenidos de formación activos"
              style={{ color: "#0f3d72" }}
            />
          </Tabs>
        </Grid>

        {tab == 0 ? (
          <Fragment>
            <Grid item xs={12} md={12}>
              <Card.Grid
                style={{
                  width: "100%",
                  textAlign: "center",
                  backgroundColor: "#ffff",
                  paddingTop: "40px",
                }}
              >
                <Title level={3}>Crea contenido de formación</Title>
                <Form
                  name="basic"
                  labelCol={{ span: 4 }}
                  wrapperCol={{ span: 14 }}
                  layout="horizontal"
                  size="large"
                  onFinish={onFinish}
                  onFinishFailed={onFinishFailed}
                >
                  <Form.Item label="Título" name="titulo">
                    <Input placeholder="Ingresa el titulo"/>
                  </Form.Item>
                  <Form.Item label="Subtitulo" name="subtitulo">
                    <Input placeholder="Ingresa el subtitulo" />
                  </Form.Item>

                  <Form.Item label="Link de video" name="video">
                    <Input placeholder="Ingresa el link del video" />
                  </Form.Item>

                  <Form.Item label="editor" style={{ marginBottom: "15vh" }}>
                    <ReactQuill
                      theme="snow"
                      modules={modules}
                      style={{ height: "42vh" }}
                      value={editText} 
                      onChange={setEditText}
                    />
                  </Form.Item>

                  <Button size="large" htmlType="submit" loading={loading}>
                    Guardar
                  </Button>
                </Form>
              </Card.Grid>
            </Grid>
          </Fragment>
        ) : (
          <Fragment>
            <Grid container spacing={3}>
              {formVisibile ? (
                <div key={dataDrawer._id}>
                  <DrawerContenido
                    props={dataDrawer}
                    isVisible={formVisibile}
                    onClick={onCancelView}
                    onLoad={loadData}
                  />
                </div>
              ) : null}
              <Grid item xs={12}>
                <div className="shadow overflow-hidden border-b border-gray-200 sm:rounded-lg">
                  <table className="w-full divide-y divide-gray-200">
                    <thead style={{ backgroundColor: "#1E293B" }}>
                      <tr>
                        <th
                          scope="col"
                          className="px-2 py-3 text-left text-xs font-medium text-white uppercase tracking-wider"
                        >
                          Titulo
                        </th>
                        <th
                          scope="col"
                          className="px-6 py-3 text-left text-xs font-medium text-white uppercase tracking-wider"
                        >
                          Detalle
                        </th>
                        <th scope="col" className="relative px-6 py-3">
                          <span className="sr-only">Editar</span>
                        </th>
                      </tr>
                    </thead>
                    <tbody className="bg-white divide-y divide-gray-200">
                      {state.map((task: any) => (
                        <tr key={task._id}>
                          <td className="px-6 py-4">
                            <div className="flex items-center">
                              <div className="ml-4">
                                <div className="text-sm font-medium text-gray-900">
                                  {task.titulo}
                                </div>
                                {/* <div className="text-sm text-gray-500">
                                  {task.video}
                                </div> */}
                              </div>
                            </div>
                          </td>
                          <td className="px-6 py-3">
                            <div className="text-sm text-gray-900">
                              {ReactHtmlParser(task.detalle)}
                            </div>
                          </td>
                          <td className="px-6 py-4 whitespace-nowrap text-right text-sm font-medium">
                            <Button
                              type="link"
                              shape="circle"
                              icon={<EyeFilled />}
                              onClick={() => viewContenido(task)}
                              size="small"
                            ></Button>

                            <Popconfirm
                              title="Estás seguro que deseas eliminar este contenido?"
                              cancelText="Cancelar"
                              okText="Eliminar"
                              onConfirm={() => onDelete(task)}
                            >
                              <Button
                                type="link"
                                shape="circle"
                                icon={
                                  <CloseCircleOutlined
                                    style={{ color: "#ff0000" }}
                                  />
                                }
                                size="small"
                              ></Button>
                            </Popconfirm>
                          </td>
                        </tr>
                      ))}
                    </tbody>
                  </table>
                </div>
              </Grid>
            </Grid>
          </Fragment>
        )}
      </Grid>
    </>
  );
};

export default FormContenidos;
