import dynamic from "next/dynamic";

import { Form, Input, Button, Drawer, message } from "antd";
const ReactQuill = dynamic(() => import("react-quill"), { ssr: false });
import "react-quill/dist/quill.snow.css";
import axios from "axios";
import { useState } from "react";

const DrawerContenido = (props: any) => {
  const [loading, setLoading] = useState(false);

  const onFinish = async (values: any) => {
    const token = localStorage.getItem("token");
    setLoading(true);
    try{
      const response = await axios.put(`https://api-smc43.ondigitalocean.app/api/editarContenido/${props.props._id}`, values, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      });
      if(response.status === 200){
        message.success(response.data.msg);
        setLoading(false);
        props.onLoad()
      }
    }catch(e){
      setLoading(false);
      // console.log('error --->>', e)
    }
  };

  const onFinishFailed = (errorInfo: any) => {
    // console.log('Failed:', errorInfo);
  };

  const layout = {
    labelCol: { span: 5 },
    wrapperCol: { span: 16 },
  };

  const modules = {
    toolbar: [
      [{ header: [1, 2, false] }],
      ["bold", "italic", "underline", "strike", "blockquote"],
      [
        { list: "ordered" },
        { list: "bullet" },
        { indent: "-1" },
        { indent: "+1" },
      ],
      ["link", "image"],
      ["clean"],
    ],
  };

  return (
    <Drawer
      title="Información de el Contenido"
      width={720}
      visible={props.isVisible}
      onClose={props.onClick}
      bodyStyle={{ paddingBottom: 80 }}
    >

      <Form {...layout} name="nest-messages" onFinish={onFinish}
      onFinishFailed={onFinishFailed}>
        <Form.Item
          label="Titulo"
          rules={[{ required: true }]}
          initialValue={props.props.titulo} 
          name="titulo"
          // {...register("titulo")} 
        >
          <Input />
        </Form.Item>
        <Form.Item
          label="Subtitulo"
          initialValue={props.props.subtitulo}
          name="subtitulo"
        >
          <Input/>
        </Form.Item>
        <Form.Item
          label="Link del video"
          initialValue={props.props.video}
          name="video"
        >
          <Input placeholder="Ingresa el link del video"  />
        </Form.Item>
        <Form.Item name="detalle" label="Detalle">
          <ReactQuill
            theme="snow"
            modules={modules}
            defaultValue={props.props.detalle}
            style={{ height: "40vh" }}
            value="detalle"
          />
        </Form.Item>
        <Form.Item wrapperCol={{ ...layout.wrapperCol, offset: 5 }} style={{paddingTop: '10vh'}}>
          <Button type="primary" htmlType="submit" loading={loading}>
            Guardar
          </Button>
        </Form.Item>
      </Form>
    </Drawer>
  );
};

export default DrawerContenido;
