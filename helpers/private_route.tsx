import { useRouter } from 'next/router';


const withAuth = (WrappedComponent: any) => {
    return (props:any) => {
        if(typeof window !== 'undefined'){
            const Router = useRouter();

            const usuario = localStorage.getItem('token');

            if(!usuario){
                Router.replace("/auth/login");
                return null;
            }

            return <WrappedComponent {...props}/>
        }
        return null;
    }
}

export default withAuth;