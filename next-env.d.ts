declare module '*.jpg';
declare module '*.jpeg';
declare module '*.png';
declare module '*.svg';

declare module '*.scss' {
  const classes: { [key: string]: string};
  export default classes;
}
declare module '*.css' {
  const classes: { [key: string]: string};
  export default classes;
}

declare module '@styled-system/theme-get' {
  export function themeGet(path: string, fallback?: any): any;
}


declare module 'react-loadingg';
declare module 'styled-theme';
declare module 'react-to-pdf';