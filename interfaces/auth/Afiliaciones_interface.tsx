export interface IFirma {
    alternativeText: string,
    caption: string,
    createdAt: string,
    ext: string,
    url: string
}

export interface IAfiliaciones{
    ciudad: string,
    correo: string,
    direccion: string,
    edad: number,
    firma: IFirma,
    id: string,
    nombre: string,
    numero_documento:string,
    published_at: string,
    telefono_fijo: string,
    telefono_movil: string,
    tipo_documento: string
}